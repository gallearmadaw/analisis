
exports.up = function(knex) {
    return knex.schema.createTable('jabatan',function(table){
        table.increments('id').unsigned().primary();
        table.string('kode_jabatan');
        table.string('nama_jabatan');
        table.integer('id_dinas').unsigned();
        table.foreign('id_dinas').references('dinas.id');
        table.integer('id_parent').unsigned();
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('modified_at').defaultTo(knex.fn.now());
    })
};

exports.down = function(knex) {
  return knex.raw('DROP TABLE jabatan')
};
