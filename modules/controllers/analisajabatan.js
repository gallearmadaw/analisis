'use strict';

/* Utils, Library */

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var config = require('../../tools/config');
var fs = require('fs');
var mime = require('mime-types');
var moment = require('moment');
var http = require('http');
var url = require('url');
var Utils = require('../utils/Utils');
var db = require('../../knex/knex');

/**
 * @route POST /analisajabatan
 * @group Analisa Jabatan
 * @param {integer} id_jabatan.query.required - Id Jabatan
 * @param {string} ringkasan_tugas.query.required - Ringkasan Tugas
 * @param {string} hasil_kerja.query - Hasil Kerja
 * @param {string} bahan_kerja.query - Bahan Kerja
 * @param {string} peralatan_kerja.query - Peralatan kerja
 * @param {string} tanggung_jawab.query - Tanggung Jawab
 * @param {string} wewenang.query - Wewenang
 * @param {string} korelasi_jabatan.query - Korelasi Jabatan
 * @param {string} keadaan_tempatkerja.query - Keadaan tempat kerja
 * @param {string} upaya_fisik.query - Upaya Fisik
 * @param {string} resiko_bahaya.query - Resiko Bahaya
 * @param {string} pangkat.query - Pangkat
 * @param {string} keahlian.query - Keahlian
 * @param {string} ketrampilan.query - Ketrampilan
 * @param {string} pengetahuan.query - Pengetahuan
 * @param {string} pendidikan.query - Pendidikan
 * @param {string} pelatihan.query - Pelatihan
 * @param {string} pengalaman_kerja.query - Pengalaman kerja
 * @param {string} kondisi_fisik.query - Kondisi fisik
 * @param {string} kondisi_bakatkerja.query - Kondisi mental bakat kerja
 * @param {string} kondisi_temperamen.query - Kondisi Temperamen
 * @param {string} mental_kerja.query - Mental kerja
 * @returns {object} 200 - { "success": true, "message": "Message Success, Message Error", "data": "if any, could be object / json" }
 * @produces application/json
 * @consumes application/x-www-form-urlencoded
 * @security JWT 
 */

exports.analisajabatan = (req, res) => {
    var body;
    var data = {
        id_jabatan : req.query.id_jabatan,
        ringkasan_tugas : req.query.ringkasan_tugas,
        hasil_kerja : req.query.hasil_kerja,
        bahan_kerja : req.query.bahan_kerja,
        peralatan_kerja : req.query.peralatan_kerja,
        tanggung_jawab : req.query.tanggung_jawab,
        wewenang : req.query.wewenang,
        korelasi_jabatan : req.query.korelasi_jabatan,
        keadaan_tempatkerja : req.query.keadaan_tempatkerja,
        upaya_fisik : req.query.upaya_fisik,
        resiko_bahaya : req.query.resiko_bahaya,
        pangkat : req.query.pangkat,
        keahlian : req.query.keahlian,
        ketrampilan : req.query.ketrampilan,
        pengetahuan : req.query.pengetahuan,
        pendidikan : req.query.pendidikan,
        pelatihan : req.query.pelatihan,
        pengalaman_kerja : req.query.pengalaman_kerja,
        kondisi_fisik:req.query.kondisi_fisik,
        kondisi_mental_bakatkerja : req.query.kondisi_bakatkerja,
        kondisi_mental_temperamentkerja : req.query.kondisi_temperamen,
        minat_kerja : req.query.minat_kerja        
    }
    db('analisajabatan').insert(data)
        .then((response) => {
            body = { success: true, message: 'Success', data: response }
        })
        .catch((error) => {
            body = { success: false, message: 'Error Insert', data: error.message }
        })
        .finally(() => {
            Utils.sendStatus(res, 200, body);
        })
}


/**
 * @route GET /analisajabatanbyid
 * @group Analisa Jabatan
 * @param {integer} id.query  - Id Analisa Jabatan
 * @returns {object} 200 - { "success": true, "message": "Message Success, Message Error", "data": "if any, could be object / json" }
 * @produces application/json
 * @consumes application/x-www-form-urlencoded
 * @security JWT 
 */

exports.analisajabatanbyid = (req, res) => {
    var body;
    db('analisajabatan').select('*')
        .then((response) => {
            body = { success: true, message: 'Success', data: response };
        })
        .catch((error) => {
            body = { success: false, message: 'Not Found', data: error.message };
        })
        .finally(() => {
            Utils.sendStatus(res, 200, body);
        })
}