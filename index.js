var compression = require('compression');
var helmet = require('helmet');
var config = require('./tools/config');
var cors = require('cors')
var express = require('express'),
    app = express(),
    port = process.env.PORT || config.server_port,
    bodyParser = require('body-parser');
var expressSwagger = require('express-swagger-generator')(app);
let options = {
    swaggerDefinition: {
        info: {
            description: 'Analisis API Documentation',
            title: 'Swagger',
            version: '1.0.0',
        },
        host: config.server+':'+config.server_port,
        basePath: '/',
        produces: [
            "application/json"
        ],
        schemes: ['http', 'https'],
        securityDefinitions: {
            JWT: {
                type: 'apiKey',
                in: 'header',
                name: 'access-token',
                description: "",
            }
        }
    },
    basedir: __dirname,
    files: ['./modules/**/*.js']
};

app.use(cors())
app.use(compression());
app.use(helmet());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


var routes = require('./modules/routes/routes');
routes(app);
var server = app.listen(port, function() {
    console.log('Express server listening on port ' + port);
});
expressSwagger(options)