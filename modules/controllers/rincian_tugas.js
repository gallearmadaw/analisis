'use strict';

/* Utils, Library */

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var config = require('../../tools/config');
var fs = require('fs');
var mime = require('mime-types');
var moment = require('moment');
var http = require('http');
var url = require('url');
var Utils = require('../utils/Utils');
var db = require('../../knex/knex');


/**
 * @route POST /rinciantugasadd
 * @group Rincian Tugas
 * @param {string} rinciantugas.query.required - Rincian Tugas
 * @param {integer} id_analisajabatan.query - Id Analisa Jabatan
 * @returns {object} 200 - { "success": true, "message": "Message Success, Message Error", "data": "if any, could be object / json" }
 * @produces application/json
 * @consumes application/x-www-form-urlencoded
 * @security JWT 
 */

exports.rinciantugasadd = (req, res) => {
    var body;
    var data = {
        rinciantugas: req.query.rinciantugas,
        id_analisajabatan: req.query.id_analisajabatan
    }
    db('rincian_tugas').insert(data)
        .then((response) => {
            body = { success: true, message: 'Success', data: response }
        })
        .catch((error) => {
            body = { success: false, message: 'Error Insert', data: error.message }
        })
        .finally(() => {
            Utils.sendStatus(res, 200, body);
        })
}
