'use strict';

/* Utils, Library */

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var config = require('../../tools/config');
var fs = require('fs');
var mime = require('mime-types');
var moment = require('moment');
var http = require('http');
var url = require('url');
var Utils = require('../utils/Utils');
var db = require('../../knex/knex');
var sqlite = require('../../tools/sqlite');

/**
 * @route POST /signin
 * @group Users
 * @param {string} username.query.required - Username
 * @param {string} password.query.required - Password
 * @returns {object} 200 - { "success": true, "message": "Message Success, Message Error", "data": "if any, could be object / json" }
 * @produces application/json
 * @consumes application/x-www-form-urlencoded
 */

exports.signin = (req, res) => {
    var body;
    db.from('users')
        .where('username', req.query.username)
        .where('is_active', true)
        .where('is_deleted', false)
        .then((value) => {
            if (value.length > 0) {
                var passwordValid = bcrypt.compareSync(req.query.password, value[0].password);
                if (!passwordValid) {
                    body = { success: false, message: 'Password Incorrect' };
                } else {
                    var tokens = jwt.sign({ id: value[0].id }, config.secret, {
                        expiresIn: config.expiresSession
                    })

                    var bodyToken = [];

                    let sql = "SELECT * FROM active_token WHERE id_user=?";
                    sqlite.get(sql, [value[0].id], (err, row) => {
                        if (err) return Utils.sendStatus(res, 200, { success: false, message: 'Failed to Refreshed Token.' });
                        if (row) { 
                            let sqlUpdate = "UPDATE active_token SET token=? WHERE id_user=?";
                            var valueUpdate = [tokens, value[0].id];
                            let stmt = sqlite.prepare(sqlUpdate);
                            stmt.run(valueUpdate, (error) => {
                                body = { success: false, message: 'Eror update' };
                            });
                            stmt.finalize();
                        } else {
                            let sqlInsert = "INSERT INTO active_token (token, id_user) values ( ?, ?)";
                            var valueInsert = [tokens, value[0].id];
                            let stmt = sqlite.prepare(sqlInsert);
                            stmt.run(valueInsert, (error) => {
                                body = { success: false, message: 7 };
                            });
                            stmt.finalize();
                        }
                    });

                    bodyToken.push({ token: tokens, user: value[0] });
                    body = { success: true, message: 'success', data: bodyToken };
                }
            } else {
                body = { success: false, message: 'Not Found' }
            }
        }).catch((error) => {
            body = { success: false, message: 'Error Select', data: error.message }
        }).finally(() => {
            Utils.sendStatus(res, 200, body);
        })
}

/**
 * @route POST /signup
 * @group Users
 * @param {string} username.query.required - Username
 * @param {string} nama_user.query.required - Nama User
 * @param {string} password.query.required - Password
 * @param {integer} id_dinas.query.required - Id Dinas
 * @param {string} flag_akses.query.required - Flag Akses
 */

exports.signup = (req, res) => {
    var body;
    var insertData = {
        username: req.query.username,
        nama_user: req.query.nama_user,
        password: bcrypt.hashSync(req.query.password, 8),
        id_dinas: req.query.id_dinas,
        flag_akses: req.query.flag_akses,
    }
    db('users').insert(insertData)
        .then((response) => {
            body = { success: true, message: 'Succes insert', data: response }
        }).catch((error) => {
            body = { success: false, message: 'Error Insert', data: error.message }
        }).finally(() => {
            Utils.sendStatus(res, 200, body);
        })

}