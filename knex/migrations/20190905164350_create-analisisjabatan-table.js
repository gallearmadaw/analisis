
exports.up = function(knex) {
    return knex.schema.createTable('analisajabatan',function(table){
        table.increments('id').unsigned().primary();
        table.integer('id_jabatan').unsigned();
        table.foreign('id_jabatan').references('jabatan.id');
        table.string('ringkasan_tugas');
        table.string('hasil_kerja');
        table.string('bahan_kerja');
        table.string('peralatan_kerja');
        table.string('tanggung_jawab');
        table.string('wewenang');
        table.text('korelasi_jabatan');
        table.text('keadaan_tempatkerja');
        table.text('upaya_fisik');
        table.text('resiko_bahaya');
        table.text('pangkat');
        table.text('keahlian');
        table.text('ketrampilan');
        table.text('pengetahuan');
        table.text('pendidikan');
        table.text('pelatihan');
        table.text('pengalaman_kerja');
        table.text('kondisi_fisik');
        table.text('kondisi_mental_bakatkerja');
        table.text('kondisi_mental_temperamenkerja');
        table.text('minat_kerja');
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('modified_at').defaultTo(knex.fn.now());
    })
};

exports.down = function(knex) {
  return knex.raw('DROP TABLE analisajabatan');
};
