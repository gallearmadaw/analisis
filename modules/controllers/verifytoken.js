'use strict';
var jwt = require('jsonwebtoken');
var config = require('../../tools/config');
var db = require('../../knex/knex');
var Utils = require('../utils/Utils');
var sqlite = require('../../tools/sqlite');

function verifyToken(req, res, next) {
    var token = req.headers['access-token'];
    if (!token) return Utils.sendStatus(res, 200, { success: false, message: 'No Token Authorized' });


    let sql = "SELECT * FROM active_token WHERE token=?";
    sqlite.get(sql, [token], (err, row) => {
        if (err) return Utils.sendStatus(res, 200, { success: false, message: 'Unathorized Token' });
        if (row) {
            jwt.verify(token, config.secret, function(err, decoded) {
                if (err) return Utils.sendStatus(res, 200, { success: false, message: 'Unathorized Token' });
                db.from('users').select('*')
                  .where('id', decoded.id)
                  .then((value)=>{
                    if (value.length < 1) return Utils.sendStatus(res, 200, { success: false, message: 'User Not Found Token' });
                  })
                  .catch((error)=>{
                    return Utils.sendStatus(res, 200, { success: false, message: 'User Not Found Token', data:error });
                  })
                  .finally(()=>{
                    req.userId = decoded.id;    
                    next();    
                  });
              });
        } else {
            Utils.sendStatus(res, 200, { success: false, message: 'Unathorized Token' });
        }
    });

}
module.exports = verifyToken;