'use strict';

/* Utils, Library */

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var config = require('../../tools/config');
var fs = require('fs');
var mime = require('mime-types');
var moment = require('moment');
var http = require('http');
var url = require('url');
var Utils = require('../utils/Utils');
var db = require('../../knex/knex');

/**
 * @route POST /dinasadd
 * @group Dinas
 * @param {string} kode_dinas.query.required - Kode Dinas
 * @param {string} nama_dinas.query.required - Nama Dinas
 * @returns {object} 200 - { "success": true, "message": "Message Success, Message Error", "data": "if any, could be object / json" }
 * @produces application/json
 * @consumes application/x-www-form-urlencoded
 * @security JWT 
 */

exports.dinasadd = (req, res) => {
    var body;
    var data = {
        kode_dinas: req.query.kode_dinas,
        nama_dinas: req.query.nama_dinas
    }
    db('dinas').insert(data)
        .then((response) => {
            body = { success: true, message: 'Success', data: response }
        })
        .catch((error) => {
            body = { success: false, message: 'Error Insert', data: error.message }
        })
        .finally(() => {
            Utils.sendStatus(res, 200, body);
        })
}

/**
 * @route GET /dinasall
 * @group Dinas
 * @returns {object} 200 - { "success": true, "message": "Message Success, Message Error", "data": "if any, could be object / json" }
 * @produces application/json
 * @consumes application/x-www-form-urlencoded
 * @security JWT 
 */

exports.dinasall = (req, res) => {
    var body;
    db('dinas').select('*')
        .then((response) => {
            body = { success: true, message: 'Success', data: response };
        })
        .catch((error) => {
            body = { success: false, message: 'Not Found', data: error.message };
        })
        .finally(() => {
            Utils.sendStatus(res, 200, body);
        })
}

/**
 * @route PUT /dinasupdate
 * @group Dinas
 * @param {integer} id.query.required - Id dinas
 * @param {string} kode_dinas.query.required - Kode Dinas
 * @param {string} nama_dinas.query.required - Nama Dinas
 * @returns {object} 200 - { "success": true, "message": "Message Success, Message Error", "data": "if any, could be object / json" }
 * @produces application/json
 * @consumes application/x-www-form-urlencoded
 * @security JWT 
 * 
 */

exports.dinasupdate = (req, res) => {
    var body;
    var dataUpdate = {
        kode_dinas: req.query.kode_dinas,
        nama_dinas: req.query.nama_dinas,
    }
    db('dinas').update(dataUpdate)
        .where('id', req.query.id)
        .then((response) => {
            body = { success: true, message: 'Success Update', data: response };
        })
        .catch((error) => {
            body = { success: false, message: 'Not Found', data: error.message };
        })
        .finally(() => {
            Utils.sendStatus(res, 200, body);
        })
}

/**
 * @route DELETE /dinasdelete
 * @group Dinas
 * @param {integer} id.query.required - Id dinas
 * @returns {object} 200 - { "success": true, "message": "Message Success, Message Error", "data": "if any, could be object / json" }
 * @produces application/json
 * @consumes application/x-www-form-urlencoded
 * @security JWT 
 */

exports.dinasdelete = (req, res) => {
    var body;
    db('dinas')
        .where('id', req.query.id)
        .del()
        .then((response) => {
            body = { success: true, message: 'Success Delete', data: response };
        })
        .catch((error) => {
            body = { success: false, message: 'Not Found', data: error.message };
        })
        .finally(() => {
            Utils.sendStatus(res, 200, body);
        })
}