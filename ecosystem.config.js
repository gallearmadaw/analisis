module.exports = {
    apps : [{
      name: 'Analisis',
      script: 'index.js',
      args: 'Analisis',
      instances: 1,
      autorestart: true,
      watch: false,
      ignore_watch : ["uploads", "uploads/files", ".git", "node_modules", "tools/db/sqlite/workforce.db"],
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'development'
      },
      env_production: {
        NODE_ENV: 'production'
      }
    }],
    deploy : {
      production : {
        user : 'node',
        host : '212.83.163.1',
        ref  : 'origin/master',
        repo : 'git@github.com:repo.git',
        path : '/var/www/production',
        'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production'
      }
    }
  };
