
exports.up = function(knex) {
    return knex.schema.createTable('users',function(table){
        table.increments('id').unsigned().primary();
        table.string('username');
        table.string('nama_user');
        table.text('password');
        table.integer('id_dinas').unsigned();
        table.foreign('id_dinas').references('dinas.id');
        table.text('flag_akses');
        table.boolean('is_active').defaultTo(true);
        table.boolean('is_deleted').defaultTo(false);
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('modified_at').defaultTo(knex.fn.now());

    })
};

exports.down = function(knex) {
    return knex.raw('DROP table users');
};
