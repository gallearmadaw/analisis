
exports.up = function(knex) {
  return knex.schema.createTable('dinas',function(table){
      table.increments('id').unsigned().primary();
      table.string('kode_dinas');
      table.string('nama_dinas');
      table.timestamp('created_at').defaultTo(knex.fn.now());
      table.timestamp('modified_at').defaultTo(knex.fn.now());
  })
};

exports.down = function(knex) {
  return knex.raw('DROP TABLE dinas');
};
