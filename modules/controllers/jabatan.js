'use strict';

/* Utils, Library */

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var config = require('../../tools/config');
var fs = require('fs');
var mime = require('mime-types');
var moment = require('moment');
var http = require('http');
var url = require('url');
var Utils = require('../utils/Utils');
var db = require('../../knex/knex');

/**
 * @route POST /jabatanadd
 * @group Jabatan
 * @param {string} kode_jabatan.query.required - Kode Jabatan
 * @param {string} nama_jabatan.query.required - Nama Jabatan
 * @param {integer} id_dinas.query.required - Id Dinas
 * @param {integer} id_parent.query.required - Id Parent
 * @returns {object} 200 - { "success": true, "message": "Message Success, Message Error", "data": "if any, could be object / json" }
 * @produces application/json
 * @consumes application/x-www-form-urlencoded
 * @security JWT 
 */

exports.jabatanadd = (req, res) => {
    var body;
    var data = {
        kode_jabatan: req.query.kode_jabatan,
        nama_jabatan: req.query.nama_jabatan,
        id_dinas : req.query.id_dinas,
        id_parent : req.query.id_parent
    }
    db('jabatan').insert(data)
        .then((response) => {
            body = { success: true, message: 'Success', data: response }
        })
        .catch((error) => {
            body = { success: false, message: 'Error Insert', data: error.message }
        })
        .finally(() => {
            Utils.sendStatus(res, 200, body);
        })
}

/**
 * @route GET /jabatanall
 * @group Jabatan
 * @returns {object} 200 - { "success": true, "message": "Message Success, Message Error", "data": "if any, could be object / json" }
 * @produces application/json
 * @consumes application/x-www-form-urlencoded
 * @security JWT 
 */

exports.jabatanall = (req, res) => {
    var body;
    db('jabatan').select('*')
        .then((response) => {
            body = { success: true, message: 'Success', data: response };
        })
        .catch((error) => {
            body = { success: false, message: 'Not Found', data: error.message };
        })
        .finally(() => {
            Utils.sendStatus(res, 200, body);
        })
}

/**
 * @route PUT /jabatanupdate
 * @group Jabatan
 * @param {integer} id.query.required - Id Jabatan
 * @param {string} kode_jabatan.query.required - Kode Jabatan
 * @param {string} nama_jabatan.query.required - Nama Jabatan
 * @param {integer} id_dinas.query.required - Id Dinas
 * @param {integer} id_parent.query.required - Id Parent
 * @returns {object} 200 - { "success": true, "message": "Message Success, Message Error", "data": "if any, could be object / json" }
 * @produces application/json
 * @consumes application/x-www-form-urlencoded
 * @security JWT 
 * 
 */

exports.jabatanupdate = (req, res) => {
    var body;
    var dataUpdate = {
        kode_jabatan: req.query.kode_jabatan,
        nama_jabatan: req.query.nama_jabatan,
        id_dinas : req.query.id_dinas,
        id_parent : req.query.id_parent,
    }
    db('jabatan').update(dataUpdate)
        .where('id', req.query.id)
        .then((response) => {
            body = { success: true, message: 'Success Update', data: response };
        })
        .catch((error) => {
            body = { success: false, message: 'Not Found', data: error.message };
        })
        .finally(() => {
            Utils.sendStatus(res, 200, body);
        })
}

/**
 * @route DELETE /jabatandelete
 * @group Jabatan
 * @param {integer} id.query.required - Id jabatan
 * @returns {object} 200 - { "success": true, "message": "Message Success, Message Error", "data": "if any, could be object / json" }
 * @produces application/json
 * @consumes application/x-www-form-urlencoded
 * @security JWT 
 */

exports.jabatandelete = (req, res) => {
    var body;
    db('jabatan')
        .where('id', req.query.id)
        .del()
        .then((response) => {
            body = { success: true, message: 'Success Delete', data: response };
        })
        .catch((error) => {
            body = { success: false, message: 'Not Found', data: error.message };
        })
        .finally(() => {
            Utils.sendStatus(res, 200, body);
        })
}