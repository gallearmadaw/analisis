exports.sendStatus = function(res, status, body){
    res.status(status).send(body);
}

exports.getDateMillis = function(result){
    var d = new Date();
    var milliseconds = Date.parse(d);
    result(milliseconds);
}
