'use strict';
var config = require('../../tools/config');
var multer = require('multer');
var path = require('path');

var storageFiles = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, config.filePath)
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname)
    }
})

var uploadFiles = multer({ storage: storageFiles })
module.exports = function (app) {

    /** Define File Controllers */
    var users = require('../../modules/controllers/users');
    var dinas = require('../../modules/controllers/dinas');
    var jabatan = require('../../modules/controllers/jabatan');
    var analisajabatan = require('../../modules/controllers/analisajabatan');
    var rincian_tugas = require('../../modules/controllers/rincian_tugas')

    var verifytoken = require('../../modules/controllers/verifytoken');

    /* Default */
    app.route('/').get((req, res) => {
        res.send('Hello World ! *Author Gallih Armada*')
    })

    /** Users */
    app.route('/signin').post(users.signin);
    app.route('/signup').post(users.signup);

    /** Dinas */
    app.route('/dinasadd').post(verifytoken, dinas.dinasadd);
    app.route('/dinasall').get(verifytoken, dinas.dinasall);
    app.route('/dinasupdate').put(verifytoken, dinas.dinasupdate);
    app.route('/dinasdelete').delete(verifytoken, dinas.dinasdelete);

    /** Jabatan */
    app.route('/jabatanadd').post(verifytoken, jabatan.jabatanadd);
    app.route('/jabatanall').get(verifytoken, jabatan.jabatanall);
    app.route('/jabatanupdate').put(verifytoken, jabatan.jabatanupdate);
    app.route('/jabatandelete').delete(verifytoken, jabatan.jabatandelete);


    /** Analisa Jabatan */
    app.route('/analisajabatan').post(verifytoken, analisajabatan.analisajabatan);
    app.route('/analisajabatanbyid').get(verifytoken, analisajabatan.analisajabatanbyid);

    /** Rincian Tugas */
    app.route('/rinciantugasadd').post(verifytoken,rincian_tugas.rinciantugasadd);

}