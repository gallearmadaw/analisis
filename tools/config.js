module.exports = {
    'secret': 'globalapisecret',
    'versionKey': false,
    'timeoutRedis': 60,
    'expiresSession': 7*86400, //1 minggu
    'filePath': 'uploads/files',
    'dirname': '',
    
    /* config server */
    'server' : 'localhost',
    'server_port': 5000,
    
    /* config database */
    'database': 'analisis',
    'user': 'root',
    'password': '',
    'host' : 'localhost',
    'port': '3306'
};
