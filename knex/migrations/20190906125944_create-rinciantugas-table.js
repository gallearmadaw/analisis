
exports.up = function(knex) {
  return knex.schema.createTable('rincian_tugas',function(table){
      table.increments('id').unsigned().primary();
      table.string('rincian_tugas');
      table.integer('id_analisajabatan').unsigned();
      table.foreign('id_analisajabatan').references('analisajabatan.id');
      table.timestamp('created_at').defaultTo(knex.fn.now());
      table.timestamp('modified_at').defaultTo(knex.fn.now());
  })
};

exports.down = function(knex) {
  return knex.raw('DROP TABLE rincian_tugas');
};
